import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomeScreen from './screens/home/homeScreen';
import ProfileScreen from './screens/profile/profileScreen';
import AuthScreen from './screens/auth/authScreen';

const Stack = createNativeStackNavigator();

export default App = () => {
  return (<NavigationContainer>
    <Stack.Navigator initialRouteName='Auth'>
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{title: 'Home'}}
      />
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{title: 'Profile'}}
      />
      <Stack.Screen
        name="Auth"
        component={AuthScreen}
        options={{title: 'Login'}}
      />
    </Stack.Navigator>
  </NavigationContainer>);
};

// export default App;
