import * as React from 'react';
import {View, Button, Text, StyleSheet} from 'react-native';
import styles from '../../styles/mainStyle'

const HomeScreen = ({navigation}) => {
  return (
    <View style={styles.main}>
      <Text>This is home...</Text>
      <Button
        title="Go to Jane's profile"
        onPress={() => navigation.navigate('Profile', {name: 'Jane'})}
      />
    </View>
  );
};

export default HomeScreen;
