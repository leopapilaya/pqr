import React, {useState, useEffect} from 'react';
import {View, Button, Text, StyleSheet} from 'react-native';

import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import styles from '../../styles/mainStyle';

const AuthScreen = ({navigation}) => {
  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        '658340581511-866sf5uob6qjq5l4inh8m6ft322oqftj.apps.googleusercontent.com',
    });
  }, []);

  async function onGoogleButtonPress() {
    try {
      // Check if your device supports Google Play
      await GoogleSignin.hasPlayServices({showPlayServicesUpdateDialog: true});
      // Get the users ID token
      const {idToken} = await GoogleSignin.signIn();

      // Create a Google credential with the token
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      // Sign-in the user with the credential
      return auth().signInWithCredential(googleCredential);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('---> user cancelled the login flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log('---> operation (e.g. sign in) is in progress already');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log('---> play services not available or outdated');
      } else {
        // some other error happened
        console.log('---> some other error happened:', error);
      }
    }
  }

  function GoogleSignIn() {
    return (
      <Button
        title="Google Sign-In"
        onPress={() =>
          onGoogleButtonPress().then(auth => {
            if (__DEV__) {
              console.log('Signed in with Google!');
              console.log(auth.user.displayName);
            }
          })
        }
      />
    );
  }

  return <View style={styles.main}>{GoogleSignIn()}</View>;
};

export default AuthScreen;
