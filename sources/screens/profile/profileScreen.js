import * as React from "react";
import { View, Text } from "react-native";
import styles from '../../styles/mainStyle'

const ProfileScreen = ({navigation, route}) => {
    return (
        <View style={styles.main}>
            <Text>{route.params.name}'s Profile...</Text>
        </View>
    );
}

export default ProfileScreen;